from django.shortcuts import render, redirect
from datetime import datetime, time
from django.db import connection
import main.auth as auth

## ANNE
def create_donation(request, id):
    if auth.is_authenticated(request):
        if request.session["user"]["type"] == "individual":
            if request.method == "POST":
                user = request.session["user"]["email"]
                nominal = request.POST['nominal']
                paymentMethod = request.POST['paymentMethod']
                status = request.POST['status']
                prayer = request.POST['prayer']
                timestamp = datetime.now()

                with connection.cursor() as query:
                    query.execute("SET SEARCH_PATH TO SIDONA;")
                    query.execute(f"INSERT INTO DONATION VALUES ('{user}', '{timestamp}', '{nominal}', '{paymentMethod}', '{status}', '{prayer}', '{id}', '924491011');")
                    query.execute("SET SEARCH_PATH TO PUBLIC;")
                return redirect('/fundraiser/view/fundraising/all/')                
            else:
                dictio = {}
                user = request.session["user"]["email"]
                timestamp = datetime.now()
                dictio["timestamp"] = timestamp
                with connection.cursor() as query:
                    query.execute("SET SEARCH_PATH TO SIDONA;")
                    query.execute(f"SELECT email FROM INDIVIDUAL WHERE email='{user}';")
                    email = query.fetchone()
                    query.execute(f"SELECT * FROM FUNDRAISING WHERE id='{id}';")
                    title = query.fetchone()
                    query.execute("SET SEARCH_PATH TO PUBLIC;")
                    dictio["email"] = email
                    dictio["title"] = title 
                return render(request, 'create_donation.html', dictio)
        elif request.session["user"]["type"] == "organization":
            return redirect('/fundraiser/')
        else:
            return redirect('/systemadmin/')
    else:
        redirect('/login/')

## ANNE
def view_donation_data_donation(request):
    with connection.cursor() as query:
        user = request.session["user"]["email"]
        query.execute("SET SEARCH_PATH TO SIDONA;")
        query.execute(f"SELECT F.id, F.title, C.categoryname, P.status, D.timestamp FROM FUNDRAISING F, CATEGORY C, PAYMENT_STATUS P, DONATION D WHERE D.email = '{user}' AND D.fundid = F.id AND F.categoryid = C.id AND D.paymentstatusid = P.id;")
        result = query.fetchall()
        query.execute("SET SEARCH_PATH TO PUBLIC;")

        dictio = {}
        cleaned_result = []
        for donation in result:
            data = [x for x in donation]
            data[4] = str(data[4])
            cleaned_result.append(data)
        dictio['data'] = cleaned_result
    return render(request, 'view_donation_data_donation.html', dictio)

## ANNE
def view_donation_details_donation(request, timestamp):
    if auth.is_authenticated(request):
        if request.session["user"]["type"] == "individual":

            if request.method == "GET":
                dictio = {}
                user = request.session["user"]["email"]
                with connection.cursor() as query:
                    query.execute("SET SEARCH_PATH TO SIDONA;")
                    query.execute(f"SELECT * FROM DONATION WHERE email = '{user}' AND timestamp ='{timestamp}';")
                    donationView = query.fetchone()                    
                    query.execute("SET SEARCH_PATH TO PUBLIC;")
                    dictio["donationView"] = donationView
                return render(request, 'view_donation_details_donation.html', dictio)
            return render(request, 'view_donation_details_donation.html')                
        elif request.session["user"]["type"] == "organization":
            return redirect('/fundraiser/')
        else:
            return redirect('/systemadmin/')
    else:
        redirect('/login/')

## DANISH
def upload_proof_of_payment_donation(request, timestamp):
    # ..../donation/upload/proof/2021-10-11/738127382
    # kalo GET request lo serve form yang ada input buat upload proof_of_payment
    # kalo POST lo update donation dimana email = request.session["user"]["email"] dan timestamp = timestamp
    # set proof_of_payment = 'url'
    # context = {"fund_title":}
    # ambil donation yang mau di update
    if auth.is_authenticated(request):
        email = request.session["user"]["email"]
        if request.method == "POST": ## user ngirim form balik ke kita
            with connection.cursor() as query:
                query.execute("SET SEARCH_PATH TO SIDONA")

                proof_of_payment = request.POST["proof"]
                query.execute(f"UPDATE DONATION SET proof_of_payment='{proof_of_payment}' WHERE email='{email}' AND timestamp='{timestamp}'")

                query.execute("SET SEARCH_PATH TO PUBLIC")
            return redirect("/donation/view/all/")

        with connection.cursor() as query:
            query.execute("SET SEARCH_PATH TO SIDONA")

            name = request.session["user"]["name"]
            query.execute(f"SELECT * FROM DONATION WHERE email='{email}' AND timestamp='{timestamp}' ")
            donation = query.fetchone()
            fundid = donation[6]

            query.execute(f"SELECT title FROM FUNDRAISING WHERE id='{fundid}'")
            fundtitle = query.fetchone()

            context = {"fundtitle": fundtitle[0], "name": name}

            query.execute("SET SEARCH_PATH TO PUBLIC")
        return render(request, "upload_proof_of_payment_donation.html", context)
    else:
        return redirect("login")

## DANISH
def view_proof_of_payment_donation(request, timestamp):
    if auth.is_authenticated(request):
        email = request.session["user"]["email"]
        with connection.cursor() as query:
            query.execute("SET SEARCH_PATH TO SIDONA;")
            query.execute(f"SELECT proof_of_payment FROM DONATION WHERE Email = '{email}' AND Timestamp = '{timestamp}';")
            proof = query.fetchone()
            proof_url = proof[0]
            query.execute("SET SEARCH_PATH TO PUBLIC;")
        return redirect(proof_url)
    else:
        return redirect("login")