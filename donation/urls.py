from django.urls import path
from .views import *

urlpatterns = [
    path("donate/<id>/", create_donation),
    path("", view_donation_data_donation),
    path("view/all/", view_donation_data_donation),
    path("view/<timestamp>/", view_donation_details_donation),
    path("upload/proof/<timestamp>/", upload_proof_of_payment_donation),
    path("view/proof/<timestamp>/", view_proof_of_payment_donation)
]