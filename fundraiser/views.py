from django.db import connection
from django.shortcuts import redirect, render
import main.auth as auth
import random

def landing_page_fundraiser(request):
    ## [PERSONAL FUNDRAISINGS]
    ## [ALL FUNDRAISINGS]
    ## [MY PROFILE]
    return render(request, "landing_page_fundraiser.html")

## RASHAD
def register_fundraiser(request, page):
    # function ini ngereturn html form
    # cases:
    # - GET request => return form
    # - POST and PAGE 0 => carry over info and hide it
    # - POST and PAGE 1 => go to next page and create fundraiser if valid
    if request.method == "POST" and page == 0:
        email = request.POST["email"]
        password = request.POST["pass"]
        name = request.POST["name"]
        phone = request.POST["phone"]
        address = request.POST["address"]
        bank = request.POST["bank"]
        accnum = request.POST["accnum"]
        type_ = request.POST["type"]

        context = {"page": 1, "email": email, "password":password, "name": name, "phone": phone, "address":address, "bank": bank, "accnum": accnum, "type": type_}

        return render(request, "register_fundraiser.html", context)

    elif request.method == "POST" and page == 1:
        email = request.POST["email"]
        password = request.POST["pass"]
        name = request.POST["name"]
        phone = request.POST["phone"]
        address = request.POST["address"]
        bank = request.POST["bank"]
        accnum = request.POST["accnum"]
        type_ = request.POST["type"]
        repolink = request.POST["repolink"]

        with connection.cursor() as query:
            try:
                query.execute("SET SEARCH_PATH TO SIDONA")
                
                query.execute(f"INSERT INTO FUNDRAISER VALUES ('{email}','{password}', '{name}', '{phone}', '{address}', 0, '{accnum}','{bank}', '{repolink}', 0, 0, 'Belum verifikasi', NULL)")
        
                if type_ == "individual":
                    nik = request.POST["nik"]
                    dob = request.POST["dob"]
                    gender = request.POST["gender"]
                    
                    query.execute(f"INSERT INTO INDIVIDUAL VALUES ('{email}', '{nik}', '{dob}', '{gender}', 0)")
                
                elif type_ == "organization":
                    deed_number = request.POST["doen"]
                    org_name = request.POST["orgname"]
                    org_phone = request.POST["orgphone"]
                    year_of_establishment = request.POST["yoe"]

                    query.execute(f"INSERT INTO ORGANIZATION VALUES ('{email}', '{deed_number}', '{org_name}', '{org_phone}', {year_of_establishment})")
                
                query.execute("SET SEARCH_PATH TO PUBLIC")
                auth.login_user(request, email=email, password=password)
                return redirect("/fundraiser/")
            except:
                query.execute("SET SEARCH_PATH TO PUBLIC;")
                return redirect("/register/")

    context = {"page": page}
    auth.logout(request)
    return render(request, "register_fundraiser.html", context)

## RASHAD
def profile_fundraiser(request):
    # get user attrib from request.session
    # function to get the profile of fundraiser
    # after we select the fundraiser based on pk email
    # get the type of the user and we can use that
    # in the html to hide/show some fields
    if auth.is_authenticated(request):
        if request.session["user"]["type"] != "admin":
            user = request.session["user"]["email"]
            with connection.cursor() as query:
                query.execute("SET SEARCH_PATH TO SIDONA")

                if request.session["user"]["type"] == "individual":
                    query.execute(
                    f"""
                    SELECT * FROM FUNDRAISER
                    NATURAL JOIN INDIVIDUAL where email = '{user}'
                    """
                    )
                    user_data = query.fetchone()
                    query.execute(
                        f"""
                        SELECT F.id, F.title, C.categoryname
                        FROM FUNDRAISING F, WISHLIST_DONATION W, CATEGORY C
                        WHERE W.email = '{user}' AND F.id = W.fundid AND F.categoryid = C.id
                        """
                    )
                    donation_wishlist = query.fetchall()
                    context = {"data": user_data, "type": request.session["user"]["type"], "wishlist":donation_wishlist}
                elif request.session["user"]["type"] == "organization":
                    query.execute(
                    f"""
                    SELECT * FROM FUNDRAISER
                    NATURAL JOIN ORGANIZATION where email = '{user}'
                    """
                    )
                    user_data = query.fetchone()

                    context = {"data": user_data, "type": request.session["user"]["type"]}
                query.execute("SET SEARCH_PATH TO PUBLIC")

            return render(request, "profile_fundraiser.html", context)
        else:
            return redirect("/systemadmin/")
    else:
        return redirect("login")

## FAUZAN
def view_all_fundraisings_fundraiser(request):
    if auth.is_authenticated(request):
        email = request.session["user"]["email"]
        type = request.session["user"]["type"]
        with connection.cursor() as query:
            query.execute("SET SEARCH_PATH TO SIDONA;")
            query.execute(f"select * from fundraising f inner join category c on f.categoryid = c.id where f.emailuser != '{email}'")
            fundraising_all = query.fetchall()
            context = {
                'fundraising' : fundraising_all, 'type': type
            }
            query.execute("SET SEARCH_PATH TO PUBLIC;")
        return render(request,'view_all_fundraisings_fundraiser.html',context)
    else:
        return redirect('login')

## FAUZAN
def view_fundraising_detail_fundraiser(request, id):
    if auth.is_authenticated(request):
        with connection.cursor() as query:
            
            query.execute("SET SEARCH_PATH TO SIDONA;")
            query.execute(f"select * from fundraising f inner join category c on f.categoryid = c.id and f.id = '{id}';")
            fundraising_all = query.fetchall()
            if fundraising_all[-1][18] == 'kesehatan':
                query.execute(f"select * from health_fund h inner join patient p on h.patientid = p.nik inner join comorbid c on c.fundid = h.fundid where h.fundid = '{id}';")
            elif fundraising_all[-1][18] == 'rumah ibadah':
                query.execute(f"select * from houseofworship_fund hf inner join category_of_activity a on hf.activityid = a.id where hf.fundid = '{id}';")
            category_item = query.fetchall()

            query.execute(f"select information from submission_notes where fundid = '{id}';")
            notes = query.fetchall()
        


            context = {
                'fundraising' : fundraising_all, 'category' : category_item, 'notes' : notes
            }
            query.execute("SET SEARCH_PATH TO PUBLIC;")
        
        return render(request, "view_fundraising_detail_fundraiser.html", context)
    else:
        return redirect('login')

## FAUZAN
def update_fundraising_fundraiser(request,id):
    if auth.is_authenticated(request):
        with connection.cursor() as query:
            query.execute("SET SEARCH_PATH TO SIDONA;")
            query.execute(f"select * from fundraising f inner join category c on f.categoryid = c.id and f.id = '{id}';")
            fundraising_all = query.fetchall()


            query.execute("select nik from patient;")
            patient_nik = query.fetchall()
                
            query.execute(f"select information from submission_notes where fundid = '{id}';")
            notes = query.fetchall()

            query.execute("select certifnumb from houseofworship;")
            certif_numb = query.fetchall()


            if request.method == "POST":
                title = request.POST["title"]
                description = request.POST["description"]
                city = request.POST["city"]
                province = request.POST["province"]
                target = request.POST["target"]
                files = request.POST["files"]
                query.execute(f"update fundraising set title = '{title}', description = '{description}', city = '{city}', province= '{province}',amountoffundsneeded = {target}, repolink = '{files}' where id = '{id}'; ")
                if fundraising_all[-1][18] == 'kesehatan':
                    nik = request.POST["nik"]
                    disease = request.POST["disease"]
                    comorbid = request.POST["comorbid"]
                    query.execute(f"update health_fund set patientid = '{nik}', maindisease = '{disease}' where fundid = '{id}';")
                elif fundraising_all[-1][18] == 'rumah ibadah':
                    certif = request.POST["certif"]
                    activity = request.POST["activity"]
                    query.execute(f"update houseofworship_fund set houseofworship_id = {certif} where fundid = {id}")
                    query.execute(f"update category_of_activity set name = '{activity}' from houseofworship_fund h where id = h.activityid and h.fundid = '{id}';")
                query.execute("SET SEARCH_PATH TO PUBLIC;")
                return redirect("/fundraiser/view/fundraising/all/")
                

               


            query.execute("SET SEARCH_PATH TO PUBLIC;")
            context = {
                        'fundraising' : fundraising_all, 'notes' : notes, 'patient' : patient_nik, 'certif_numb' : certif_numb
                    }
        return render(request, "update_fundraising_fundraiser.html",context)
    else:
        return redirect('login')


## ANNE
def add_to_wishlist_fundraiser(request, id):
    if auth.is_authenticated(request):
        if request.session["user"]["type"] == "individual":
        # create wishlist_donation
            email = request.session["user"]["email"]
            fundid = id
            with connection.cursor() as query:
                query.execute("SET SEARCH_PATH TO SIDONA;")

                query.execute(f"INSERT INTO WISHLIST_DONATION VALUES ('{email}', '{fundid}')")

                query.execute("SET SEARCH_PATH TO PUBLIC;")
            return redirect("/fundraiser/view/fundraising/all/")
        else:
            redirect("/")
    else:
        return redirect("login")


## ANNE
def delete_from_wishlist_donation_fundraiser(request, id):
    if auth.is_authenticated(request):
        if request.session["user"]["type"] == "individual":
            email = request.session["email"]
            fundid = id
            with connection.cursor() as query:
                query.execute("SET SEARCH_PATH TO SIDONA;")
                query.execute(f"DELETE FROM WISHLIST_DONATION where email='{email}' and fundid='{fundid}'")
                query.execute("SET SEARCH_PATH TO PUBLIC;")
            return redirect("profile-fundraiser")
        else:
            return redirect("/")
    else:
        return redirect("login")