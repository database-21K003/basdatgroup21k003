from django.urls import path
from .views import *

urlpatterns = [
    path("", landing_page_fundraiser),
    path("register/<int:page>/", register_fundraiser),
    path("profile/", profile_fundraiser, name="profile-fundraiser"),
    path("view/fundraising/all/", view_all_fundraisings_fundraiser),
    path("view/fundraising/<id>/", view_fundraising_detail_fundraiser),
    path("update/fundraising/<id>/", update_fundraising_fundraiser),
    path("wishlist/add/<id>/", add_to_wishlist_fundraiser),
    path("wishlist/delete/<id>/", delete_from_wishlist_donation_fundraiser),
]