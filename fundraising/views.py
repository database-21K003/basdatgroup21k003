from django.shortcuts import render, redirect, resolve_url
from main.auth import is_authenticated
from django.db import connection
from datetime import date
## RASHAD
def landing_page_fundraising(request):
    # this function will server html 
    # containing 3 choices (buttons)
    # CREATE FUNDRAISING
    # PERSONAL FUNDRAISINGS
    # MANAGE COMORBID
    return render(request, "landing_page_fundraising.html")

## RASHAD
def create_choice_fundraising(request):
    # function ini bakal ngereturn html form
    # yang isinya choice category fundraising
    # this will redirect according to category chosen
    if is_authenticated(request):
        if request.method == "POST":
            cat = request.POST["category"]
            if cat == "kesehatan":
                return redirect("/fundraising/create/kesehatan/prompt/")
            elif cat == "rumah ibadah":
                return redirect("/fundraising/create/rumahibadah/prompt/")
            else:
                request.session["chosen_cat"] = cat
                return redirect("/fundraising/create/new/")

        with connection.cursor() as query:
            query.execute("SET SEARCH_PATH TO SIDONA")
            query.execute("SELECT * FROM CATEGORY")
            categories = query.fetchall()
            query.execute("SET SEARCH_PATH TO PUBLIC")
        return render(request,"create_choice_fundraising.html", {"categories": categories})
    else:
        return redirect("login")

## RASHAD
def create_health_or_HoW_fundraising(request, category):
    # just serve html where it prompts
    # user if they want to add an existing
    # patient/HoW or create new
    # this will accept requests from the function above
    if is_authenticated(request):
        
        if category == "kesehatan":
            return render(request, "create_health_or_HoW_fundraising.html", {"category":"kesehatan"})
        else:
            return render(request, "create_health_or_HoW_fundraising.html", {"category":"rumahibadah"})
    else:
        return redirect("login")

## RASHAD
def create_patient_or_HoW_fundraising(request, category):
    # serve html form that contains fields
    # for patient/HoW
    # this function will also accept POST requests
    # from the form and will serve the fundraising
    # form with the fields hidden
    # the form after that will send post request
    # to the actual fundraising form page
    if is_authenticated(request):
        if request.method == "POST":
            
            with connection.cursor() as query:
                query.execute("SET SEARCH_PATH TO SIDONA")

                query.execute("SELECT id FROM FUNDRAISING ORDER BY id DESC")
                next_id = int(query.fetchone()[0]) + 1

                query.execute("SET SEARCH_PATH TO PUBLIC")
            if category=="kesehatan":
                request.session["chosen_cat"] = "kesehatan"
                nik = request.POST["nik"]
                name = request.POST["name"]
                dob = request.POST["dob"]
                address = request.POST["address"]
                job = request.POST["job"]

                context = {
                    "nik": nik,
                    "name": name, 
                    "dob": dob, 
                    "address": address, 
                    "job": job,
                    "cat": request.session["chosen_cat"], "health": True, "how": False,
                    "fundid":next_id
                    }

                request.session["continue"] = True
                return render(request, "create_fundraising.html", context)

            elif category == "rumahibadah":
                request.session["chosen_cat"] = "rumah ibadah"
                cnumb = request.POST["cnumb"]
                name = request.POST["name"]
                address = request.POST["address"]
                type_ = request.POST["type"]
                with connection.cursor() as query:
                    query.execute("SET SEARCH_PATH TO SIDONA")

                    query.execute("SELECT * FROM CATEGORY_OF_ACTIVITY")
                    coa = query.fetchall()

                    query.execute("SET SEARCH_PATH TO PUBLIC")
                context = {
                    "cnumb": cnumb, 
                    "name": name, 
                    "address": address, 
                    "type": type_,
                    "coa": coa,
                    "cat": request.session["chosen_cat"], "health": False, "how": True,
                    "fundid":next_id
                    }
                request.session["continue"] = True
                return render(request, "create_fundraising.html", context)
        else:
            if category == "kesehatan":
                return render(request, "create_patient_or_HoW_fundraising.html", {"cat": "k"})
            else:
                return render(request, "create_patient_or_HoW_fundraising.html", {"cat": "r"})
    return redirect("login")

## RASHAD
def check_existing_patient_or_HoW_fundraising(request, category, context):
    # serves html to check existing patient/HoW
    # this function will also accept POST requests
    # from the form and will serve the fundraising
    # form with the fields hidden
    # the form after that will send post request
    # to the actual fundraising form page
    if is_authenticated(request):
        if request.method == "POST" and context=="searching":
            with connection.cursor() as query:
                query.execute("SET SEARCH_PATH TO SIDONA")
                
                if category == "kesehatan":
                    nik = request.POST["nik"]
                    query.execute(f"SELECT * FROM PATIENT WHERE NIK='{nik}'")
                    patient = query.fetchone()
                    if not patient:
                        return render(request, "check_existing_patient_or_HoW_fundraising.html", {"cat": "k", "searching": True, "confirming": False, 
                        "error": f"Patient with NIK '{nik}' was not found"})
                    else:
                        return render(request, "check_existing_patient_or_HoW_fundraising.html", {"cat": "k", "searching": False, "confirming": True, 
                        "patient": patient
                        ,"error": ""})
                elif category == "rumahibadah":
                    cnumb = request.POST["cnumb"]
                    query.execute(f"SELECT * FROM HOUSEOFWORSHIP WHERE CertifNumb='{cnumb}'")
                    how = query.fetchone()
                    if not how:
                        return render(request, "check_existing_patient_or_HoW_fundraising.html", {"cat": "r", "searching": True, "confirming": False, 
                        "error": f"House of Worship with certificate number {cnumb} was not found"})
                    else:
                        return render(request, "check_existing_patient_or_HoW_fundraising.html", {"cat": "r", "searching": False, "confirming": True, 
                        "how": how
                        ,"error": ""})

                query.execute("SET SEARCH_PATH TO PUBLIC")
        elif request.method == "POST" and context=="confirming":
            with connection.cursor() as query:
                query.execute("SET SEARCH_PATH TO SIDONA")

                query.execute("SELECT id FROM FUNDRAISING ORDER BY id DESC")
                next_id = int(query.fetchone()[0]) + 1

                query.execute("SET SEARCH_PATH TO PUBLIC")
            if category=="kesehatan":
                request.session["chosen_cat"] = "kesehatan"
                nik = request.POST["nik"]
                name = request.POST["name"]
                dob = request.POST["dob"]
                address = request.POST["address"]
                job = request.POST["job"]

                context = {
                    "nik": nik,
                    "name": name, 
                    "dob": dob, 
                    "address": address, 
                    "job": job,
                    "cat": request.session["chosen_cat"], "health": True, "how": False,
                    "fundid":next_id
                    }

                request.session["continue"] = True
                return render(request, "create_fundraising.html", context)

            elif category == "rumahibadah":
                request.session["chosen_cat"] = "rumah ibadah"
                
                cnumb = request.POST["cnumb"]
                name = request.POST["name"]
                address = request.POST["address"]
                type_ = request.POST["type"]
                with connection.cursor() as query:
                    query.execute("SET SEARCH_PATH TO SIDONA")

                    query.execute("SELECT * FROM CATEGORY_OF_ACTIVITY")
                    coa = query.fetchall()

                    query.execute("SET SEARCH_PATH TO PUBLIC")
                context = {
                    "cnumb": cnumb, 
                    "name": name, 
                    "address": address, 
                    "type": type_,
                    "coa": coa,
                    "cat": request.session["chosen_cat"], "health": False, "how": True,
                    "fundid":next_id
                    }
                request.session["continue"] = True
                return render(request, "create_fundraising.html", context)
        else:
            if category == "kesehatan":
                return render(request, "check_existing_patient_or_HoW_fundraising.html", {"cat": "k", "searching": True, "confirming": False, "error": ""})
            else:
                return render(request, "check_existing_patient_or_HoW_fundraising.html", {"cat": "r", "searching": True, "confirming": False, "error": ""})

    else:
        return redirect("login")

## RASHAD
def create_fundraising(request):
    # this function will serve html
    # for other categories of fundraisings
    # this function will also accept post requests
    # from the 2 functions above
    # redirect to personal fundraising list after
    if is_authenticated(request):
        if request.method == "POST":
            fundid = request.POST["fundid"]
            email = request.POST["email"]
            title = request.POST["title"]
            desc = request.POST["desc"]
            title = request.POST["title"]
            city = request.POST["city"]
            province = request.POST["province"]
            lastactive = request.POST.get("lastactive")
            target = request.POST["target"]
            repolink = request.POST["repolink"]
            cat = request.session.get("chosen_cat")

            with connection.cursor() as query:
                query.execute("SET SEARCH_PATH TO SIDONA")

                query.execute(f"SELECT id FROM CATEGORY WHERE categoryname='{cat}'")
                catid = query.fetchone()[0]

                query.execute(
                f"""
                INSERT INTO FUNDRAISING VALUES 
                ('{fundid}', '{title}', '{desc}', '{city}', '{province}', '{repolink}', 'Belum verifikasi', '{date.today()}', NULL, '{lastactive}', {target}, NULL, NULL, NULL, '{email}', NULL, '{catid}')
                """
                )
            
                if request.session.get("continue"):
                    if request.session.get("chosen_cat") == "kesehatan":
                        # healt_fund
                        nik = request.POST["nik"]
                        name = request.POST["name"]
                        dob = request.POST["dob"]
                        address = request.POST["address"]
                        job = request.POST["job"]
                        mdisease = request.POST["mdisease"]

                        query.execute(f"SELECT * FROM PATIENT WHERE nik='{nik}'")
                        patient = query.fetchone()
                    
                        if not patient:
                            query.execute(
                                f"""
                                INSERT INTO PATIENT VALUES
                                ('{nik}', '{name}', '{dob}', '{address}', '{job}')
                                """
                            )

                        query.execute(
                            f"""
                            INSERT INTO HEALTH_FUND VALUES
                            ('{fundid}', '{mdisease}', '{nik}')
                            """
                        )
                                  
                    else:
                        # how fund
                        cnumb = request.POST["cnumb"]
                        name = request.POST["name"]
                        address = request.POST["address"]
                        type_ = request.POST["type"]
                        coa = request.POST["coa"]

                        query.execute(f"SELECT * FROM HOUSEOFWORSHIP WHERE CertifNumb='{cnumb}'")
                        how = query.fetchone()
                        if not how:
                            query.execute(
                                f"""
                                INSERT INTO HOUSEOFWORSHIP VALUES
                                ('{cnumb}', '{name}', '{address}', '{type_}')
                                """
                            )
                        query.execute(
                            f"""
                            INSERT INTO HOUSEOFWORSHIP_FUND VALUES
                            ('{fundid}', '{cnumb}', '{coa}')
                            """
                        )

                    del request.session["continue"]

                query.execute("SET SEARCH_PATH TO PUBLIC")

            del request.session["chosen_cat"]
            return redirect("/fundraising/view/all/")
        with connection.cursor() as query:
            query.execute("SET SEARCH_PATH TO SIDONA")

            query.execute("SELECT id FROM FUNDRAISING ORDER BY id DESC")
            next_id = int(query.fetchone()[0]) + 1

            query.execute("SET SEARCH_PATH TO PUBLIC")
        context = {"cat": request.session["chosen_cat"], "health": False, "how": False, "fundid": next_id}
        
        return render(request, "create_fundraising.html", context)
    else:
        return redirect("login")

## RASHAD
def personal_fundraisings_list_fundraising(request):
    if is_authenticated(request):
        with connection.cursor() as query:
            query.execute("SET SEARCH_PATH TO SIDONA")
            email = request.session["user"]["email"]
            query.execute(
                f"""
                SELECT F.id, F.title, F.city, F.province, F.initialactivedate, F.lastactivedate, F.remainingdays, F.amountoffundsneeded, C.categoryname, F.verificationstatus
                FROM FUNDRAISING F, CATEGORY C
                WHERE F.emailuser='{email}' AND F.categoryid = C.id
                """
            )
            all_funds = query.fetchall()
            
            query.execute("SET SEARCH_PATH TO PUBLIC")
        return render(request, "personal_fundraisings_list_fundraising.html", {"all_funds": all_funds})
    else:
        return redirect("login")

## RASHAD
def delete_personal_fundraising(request, id):
    if is_authenticated(request):
        with connection.cursor() as query:
            query.execute("SET SEARCH_PATH TO SIDONA")
            email = request.session["user"]["email"]
            query.execute(f"DELETE FROM FUNDRAISING WHERE emailuser='{email}' AND id='{id}'")

            query.execute("SET SEARCH_PATH TO PUBLIC")
        return redirect("/fundraising/view/all/")
    else:
        return redirect("login")

## RASHAD
def view_personal_fundraising_detail_fundraising(request, id):
    # view details of personal fundraising
    if is_authenticated(request):
        with connection.cursor() as query:
            context = {}
            query.execute("SET SEARCH_PATH TO SIDONA")
            query.execute(
                f"""
                SELECT F.id, F.emailuser, F.title, F.description, F.city, F.province, F.lastactivedate, F.amountoffundsneeded, C.categoryname, F.repolink, F.amountoffundscollected, F.amountoffundsused
                FROM FUNDRAISING F, CATEGORY C
                WHERE F.id = '{id}' AND F.categoryid = C.id
                """
            )
            data = list(query.fetchone())

            
            context["comorbid"] = None

            if data[8] == "kesehatan":
                query.execute(
                    f"""
                    SELECT P.NIK, P.name, H.maindisease
                    FROM PATIENT P, HEALTH_FUND H
                    WHERE H.fundid='{id}' AND H.patientid= P.NIK
                    """
                )
                patient_data = list(query.fetchone())
                data.extend(patient_data)
                query.execute(f"SELECT Comorbid FROM COMORBID WHERE fundid='{id}'")
                comorbid = query.fetchall()
                context["comorbid"] = comorbid

            if data[8] == "rumah ibadah":
                query.execute(
                    f"""
                    SELECT H.certifnumb, A.name
                    FROM HOUSEOFWORSHIP_FUND H, CATEGORY_OF_ACTIVITY A
                    WHERE H.fundid='{id}' AND H.activityid = A.id
                    """
                )
                how_data = list(query.fetchone())
                data.extend(how_data)

            query.execute(
                f"""
                SELECT * FROM SUBMISSION_NOTES WHERE fundid='{id}'
                """
            )
            sub_notes = query.fetchall()
            context["notes"] = sub_notes

            query.execute(
                f"""
                SELECT email, timestamp, amount, prayer FROM DONATION WHERE fundid='{id}'
                """
            )
            donations = query.fetchall()
            context["donations"] = donations

            query.execute(
                f"""
                SELECT timestamp, nominalused, description FROM HISTORY_OF_FUND_USE WHERE fundid='{id}'
                """
            )
            history = query.fetchall()
            context["history"] = history

            context["data"] = data
            query.execute("SET SEARCH_PATH TO PUBLIC")
            return render(request, "view_personal_fundraising_detail_fundraising.html", context)
    else:
        return redirect("login")
    

## RASHAD
def manage_comorbid_diseases_fundraising(request):
    if is_authenticated(request):
        with connection.cursor() as query:
            query.execute("SET SEARCH_PATH TO SIDONA")
            email = request.session["user"]["email"]
            query.execute(
                f"""
                SELECT F.id, F.title, H.maindisease
                FROM FUNDRAISING F, HEALTH_FUND H WHERE F.emailuser='{email}' AND F.id = H.fundid
                """
            )
            raw = query.fetchall()
            data = [list(x) for x in raw]
            comorbids = []
            for i in data:
                query.execute(
                    f"""
                    SELECT comorbid from comorbid where fundid='{i[0]}'
                    """
                    )
                comorbids_found = query.fetchall()
                acc = []
                for c in comorbids_found:
                    acc.append(c[0])
                comorbids.append(acc)
            
            
            counter = 0
            for d in data:
                d.append(comorbids[counter])
                counter +=1  

            query.execute("SET SEARCH_PATH TO PUBLIC")

            return render(request, "manage_comorbid_diseases_fundraising.html", {"data": data})
    else:
        return redirect("login")

## RASHAD
def create_comorbid_fundraising(request):
    # select all fundraisings yang health yang punya user
    # terus taro di dropdown
    # this function will serve a form (GET and POST)
    if is_authenticated(request):
        if request.method == "POST":
            id = request.POST["fundid"]
            comorbid = request.POST["comorbid"]
            with connection.cursor() as query:
                query.execute("SET SEARCH_PATH TO SIDONA")

                query.execute(f"INSERT INTO COMORBID VALUES ('{id}', '{comorbid}')")

                query.execute("SET SEARCH_PATH TO PUBLIC")
            return redirect("/fundraising/view/comorbid/all/")
        email = request.session["user"]["email"]
        with connection.cursor() as query:
            query.execute("SET SEARCH_PATH TO SIDONA")
            query.execute(
                f"""
                SELECT F.id from fundraising F,health_fund H where F.emailuser='{email}' AND F.id = H.fundid
                """
            )
            funds = query.fetchall()
            query.execute("SET SEARCH_PATH TO PUBLIC")
        return render(request, "create_comorbid_fundraising.html", {"funds": funds})
    else:
        return redirect("login")

## RASHAD
def update_comorbid_fundraising(request, id, disease):
    if is_authenticated(request):
        if request.method == "POST":
            with connection.cursor() as query:
                new_disease = request.POST["c"]
                query.execute("SET SEARCH_PATH TO SIDONA")
                query.execute(f"UPDATE COMORBID set comorbid='{new_disease}' WHERE fundid='{id}' and comorbid='{disease}'")
                query.execute("SET SEARCH_PATH TO PUBLIC")
            return redirect("/fundraising/view/comorbid/all/")

        with connection.cursor() as query:
            query.execute("SET SEARCH_PATH TO SIDONA")

            query.execute(f"SELECT * from comorbid where fundid='{id}' and comorbid='{disease}'")
            comorbid = query.fetchone()
            query.execute("SET SEARCH_PATH TO PUBLIC")
            return render(request, "update_comorbid_fundraising.html", {"c": comorbid})
    else:   
        return redirect("login")
## RASHAD
def delete_comorbid_fundraising(request, id, disease):
    if is_authenticated(request):
        with connection.cursor() as query:
            query.execute("SET SEARCH_PATH TO SIDONA")

            query.execute(f"DELETE FROM COMORBID WHERE fundid='{id}' and comorbid='{disease}'")

            query.execute("SET SEARCH_PATH TO PUBLIC")
            return redirect('/fundraising/view/comorbid/all/')
    else:
        return redirect("login")

## BRYANT
def withdraw_funds_from_fundraising(request, id):
    if is_authenticated(request):
        if request.session["user"]["type"] == 'individual' or request.session["user"]["type"] == 'organization':
            if request.method == "POST":
                data = {}
                data["error"] = ""
                nominal = request.POST["nominal"]
                description = request.POST["description"]

                with connection.cursor() as query:
                    try:
                        query.execute("SET SEARCH_PATH TO SIDONA;")
                        query.execute(f"SELECT * FROM update_funds_and_saldo(%s,%s,%s)",[id,nominal,description])
                        ## TODO +amountoffundsused
                        query.execute("SET SEARCH_PATH TO PUBLIC;")
                    except:
                        data = {}
                        data["error"] = "There was a problem with the withdrawing funds either yours or fundraising balances wasn't enough"
                        user = request.session["user"]["email"]

                        with connection.cursor() as query:
                            query.execute("SET SEARCH_PATH TO SIDONA;")
                            query.execute(f"SELECT * FROM FUNDRAISING WHERE id = '{id}' AND EmailUser = '{user}';")
                            fundraising = query.fetchone()
                            query.execute(f"SELECT DonaPaySaldo FROM FUNDRAISER WHERE email = '{user}'")
                            donapay = query.fetchone()
                            query.execute("SET SEARCH_PATH TO PUBLIC;")
                            data['fundraising'] = fundraising
                            data['donapay'] = donapay
                        return render(request, "withdraw_funds_from_fundraising.html", data)    

                return redirect("/fundraising/view/all/")
            else:
                data = {}
                data["error"] = ""
                user = request.session["user"]["email"]

                with connection.cursor() as query:
                    query.execute("SET SEARCH_PATH TO SIDONA;")
                    query.execute(f"SELECT * FROM FUNDRAISING WHERE id = '{id}' AND EmailUser = '{user}';")
                    fundraising = query.fetchone()
                    query.execute(f"SELECT DonaPaySaldo FROM FUNDRAISER WHERE email = '{user}'")
                    donapay = query.fetchone()
                    query.execute("SET SEARCH_PATH TO PUBLIC;")
                    data['fundraising'] = fundraising
                    data['donapay'] = donapay
                return render(request, "withdraw_funds_from_fundraising.html", data)     
  
        else:
            return redirect("/systemadmin/")
    else:
        redirect("/login/")