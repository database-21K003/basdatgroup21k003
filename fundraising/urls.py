from django.urls import path
from .views import *

urlpatterns = [
    path('', landing_page_fundraising, name="fundraising"),
    path('create/', create_choice_fundraising),
    path('create/<category>/prompt/', create_health_or_HoW_fundraising),
    path('create/<category>/new/', create_patient_or_HoW_fundraising),
    path('create/<category>/check/<context>/', check_existing_patient_or_HoW_fundraising),
    path('create/new/', create_fundraising),
    path('view/all/', personal_fundraisings_list_fundraising),
    path('view/<id>/', view_personal_fundraising_detail_fundraising),
    path('delete/<id>/', delete_personal_fundraising),
    path('view/comorbid/all/', manage_comorbid_diseases_fundraising),
    path('create/comorbid/', create_comorbid_fundraising),
    path('update/comorbid/<id>/<disease>/', update_comorbid_fundraising),
    path('delete/comorbid/<id>/<disease>/', delete_comorbid_fundraising),
    path('withdraw/funds/<id>/', withdraw_funds_from_fundraising)
]