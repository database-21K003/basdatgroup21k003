from django.shortcuts import render, redirect
from django.db import connection
import main.auth as auth

def home(request):
    return render(request, 'main/home.html')

## RASHAD
def login_view(request):
    if auth.is_authenticated(request):
        user_type = request.session["user"]["type"]
        if user_type == "individual" or user_type == "organization":
            return redirect("/fundraiser/")
        else:
            return redirect("/systemadmin/")
        # redirect ke page yang sesuai type user
    if request.method == "POST":
        email = request.POST["email"]
        password = request.POST["pass"]
        auth.login_user(request, email=email, password=password)
        
        if not auth.is_authenticated(request):
            return render(request, "login_view.html")
            
        print(request.session["user"])
        user_type = request.session["user"]["type"]
        if user_type == "individual" or user_type == "organization":
            return redirect("/fundraiser/")
        else:
            return redirect("/systemadmin/")

    return render(request, "login_view.html")

## RASHAD
def logout_view(request):
    auth.logout(request)
    return redirect("/")

## RASHAD
def register_choice_view(request):
    # function buat ngereturn html
    # dimana user bisa milih mau register
    # sebagai admin atau fundraiser
    return render(request, "register_choice_view.html")