from django.db import connection
## authentication helper

def is_authenticated(request):
    with connection.cursor() as query:
        query.execute(f"SET SEARCH_PATH TO PUBLIC")
    return "user" in request.session

def login_user(request, email, password):
    with connection.cursor() as query:
        query.execute(f"SET SEARCH_PATH TO SIDONA")
        query.execute(f"SELECT * FROM FUNDRAISER WHERE email='{email}' AND password='{password}';")
        user_info = query.fetchall()
        if not user_info:
            query.execute(f"SELECT * FROM ADMIN WHERE email='{email}' AND password='{password}';")
            user_info = query.fetchall()
            if not user_info:
                return
            query.execute(f"SET SEARCH_PATH TO PUBLIC")
            request.session["user"] = dict(zip(["email","password","name","phone_number","emp_id"],list(user_info[0])))
            request.session["user"]["type"] = "admin"
            return
        query.execute(f"SET SEARCH_PATH TO PUBLIC")
        request.session["user"] = dict(
            zip([
                "email",
                "password",
                "name",
                "phone_number",
                "address",
                "dona_pay_saldo",
                "account_numb",
                "bank_name",
                "repo_link",
                "numb_of_fund",
                "numb_of_active_fund",
                "verification_status",
                "email_admin"
                ],list(user_info[0])))
        query.execute(f"SET SEARCH_PATH TO SIDONA")
        query.execute(f"SELECT * FROM INDIVIDUAL WHERE email='{email}'")
        user = query.fetchall()
        query.execute(f"SET SEARCH_PATH TO PUBLIC")
        if user:
            request.session["user"]["type"] = "individual"
        else:
            request.session["user"]["type"] = "organization"
        return

def logout(request):
    with connection.cursor() as query:
        query.execute(f"SET SEARCH_PATH TO PUBLIC")
    request.session.clear()