from django.urls import path
from .views import *
urlpatterns = [
    path("", landing_page_admin),
    path("register/", register_admin),
    path("view/fundraiser/all/", view_registered_fundraisers_admin),
    path("view/fundraiser/<email>/", view_registered_fundraiser_profile_admin),
    path("verify/<email>/", verify_fundraiser_admin),
    path("view/fundraising/all/", view_all_fundraisings_admin),
    path("view/fundraising/<id>/", view_fundraising_detail_admin),
    path("verify/fundraising/<id>/", verify_fundraising_page_admin),
    path("verify/fundraising/<id>/<status>/", verify_fundraising_admin),
    path("view/donations/all/", view_all_user_donation_data_admin),
    path("verify/donation/<email>/<timestamp>/", verify_donation_payment_admin),
    path("view/donation/<email>/<timestamp>/", view_detail_user_donation_data_admin),
    path("view/donation/proof/<email>/<timestamp>/", view_proof_of_payment_user_admin),
    path("view/category/all/", view_fundraising_categories_admin),
    path("create/category/", add_new_fundraising_category_admin),
    path("update/category/<id>/",update_existing_fundraising_category_admin),
]