from django.shortcuts import redirect, render
from django.db import connection
import main.auth as auth

def landing_page_admin(request):
    # return HTML yang bisa redirect ke
    # All Registered Users
    # All Fundraisings
    # All Fundraising Categories
    return render(request, "landing_page_admin.html")

## RASHAD
def register_admin(request):
    if auth.is_authenticated(request):
        auth.logout(request)
    # function buat ngereturn html
    # dimana ada form buat register
    # function ini nerima 2 tipe request
    # POST and GET requests
    if request.method == "POST":
        email = request.POST["email"]
        password = request.POST["pass"]
        name = request.POST["name"]
        phone = request.POST["phone"]

        
        with connection.cursor() as query:
            try:
                query.execute("SET SEARCH_PATH TO SIDONA;")
                query.execute("SELECT * FROM ADMIN ORDER BY empid DESC;")

                highest_id = query.fetchone()[4]
                next_id = str(int(highest_id) + 1)

                query.execute(
                    f"INSERT INTO ADMIN VALUES ('{email}', '{password}', '{name}', '{phone}', '{next_id}')"
                )
                auth.login_user(request, email=email, password=password)
                query.execute("SET SEARCH_PATH TO PUBLIC;")
            except:
                query.execute("SET SEARCH_PATH TO PUBLIC;")
                return redirect("/register/")

        
        return redirect("/systemadmin/")

    return render(request, "register_admin.html")

## RASHAD
def view_registered_fundraisers_admin(request):
    # return html table of all users
    if auth.is_authenticated(request):
        user = request.session["user"]
        if user["type"] == "admin":
            with connection.cursor() as query:
                query.execute("SET SEARCH_PATH TO SIDONA")

                query.execute("SELECT email, name, verificationstatus FROM FUNDRAISER NATURAL JOIN INDIVIDUAL")
                individuals = query.fetchall()

                query.execute("SELECT email, name, verificationstatus FROM FUNDRAISER NATURAL JOIN ORGANIZATION")
                organizations = query.fetchall()

                query.execute("SET SEARCH_PATH TO PUBLIC")

                context = {"individuals": individuals, "organizations": organizations}

            return render(request, "view_registered_fundraisers_admin.html", context)
        else:
            return redirect("/fundraiser/")    
    return redirect("login")

## RASHAD
def verify_fundraiser_admin(request, email):
    # this function will verify fundraiser
    # based on email
    # return/redirect back to registered
    # fundraisers list
    if auth.is_authenticated(request):
        user = request.session["user"]
        if user["type"] == "admin":
            with connection.cursor() as query:
                query.execute("SET SEARCH_PATH TO SIDONA")

                query.execute(f"UPDATE FUNDRAISER SET verificationstatus='Terferifikasi', emailadmin='{user['email']}' WHERE email='{email}'")

                query.execute("SET SEARCH_PATH TO PUBLIC")

                return redirect("/systemadmin/view/fundraiser/all/")
    
    return redirect("logout")

## RASHAD
def view_registered_fundraiser_profile_admin(request, email):
    # this function will actually be the same
    # as fundraiser.views.profile_fundraiser
    # the only difference is that we dont show donapaybalance
    if auth.is_authenticated(request):
        user = request.session["user"]
        if user["type"] == "admin":
            with connection.cursor() as query:
                query.execute("SET SEARCH_PATH TO SIDONA")
                query.execute(
                f"""
                SELECT * FROM FUNDRAISER
                NATURAL JOIN INDIVIDUAL where email = '{email}'
                """)
                user_data = query.fetchone()
                context = {"data":user_data, "type":"individual"}
                if not user_data:
                    query.execute(
                    f"""
                    SELECT * FROM FUNDRAISER
                    NATURAL JOIN ORGANIZATION where email = '{email}'
                    """)
                    context = {"data":user_data, "type":"organization"}
                query.execute("SET SEARCH_PATH TO PUBLIC")
                
            return render(request, "view_registered_fundraiser_profile_admin.html", context)
    else:
        return redirect("login")

## FAUZAN
def view_all_fundraisings_admin(request):
    if auth.is_authenticated(request):
        if request.session["user"]["type"] == 'admin':
            with connection.cursor() as query:
                query.execute("SET SEARCH_PATH TO SIDONA;")
                query.execute("select * from fundraising f inner join category c on f.categoryid = c.id order by f.verificationstatus,f.id;")
                fundraising_all = query.fetchall()
                context = {
                    'fundraising' : fundraising_all
                }
                query.execute("select submitdate from fundraising where id = '770600412';")
                query.execute("SET SEARCH_PATH TO PUBLIC;")
            
            return render(request, "view_all_fundraisings_admin.html", context)
        else:
            return redirect('/')
    else:
        return redirect('login')

## FAUZAN
def view_fundraising_detail_admin(request, id):
    if auth.is_authenticated(request):
        if request.session["user"]["type"] == 'admin':
            with connection.cursor() as query:
                query.execute("SET SEARCH_PATH TO SIDONA;")
                query.execute(f"select * from fundraising f inner join category c on f.categoryid = c.id and f.id = '{id}';")
                fundraising_all = query.fetchall()

                if fundraising_all[-1][18] == 'kesehatan':
                    query.execute(f"select * from health_fund h inner join patient p on h.patientid = p.nik inner join comorbid c on c.fundid = h.fundid where h.fundid = '{id}';")
                elif fundraising_all[-1][18] == 'rumah ibadah':
                    query.execute(f"select * from houseofworship_fund hf inner join category_of_activity a on hf.activityid = a.id where hf.fundid = '{id}';")
                category_item = query.fetchall()

                query.execute(f"select * from donation d inner join payment_status p on d.paymentstatusid = p.id and d.fundid = '{id}';")
                donation = query.fetchall()


                query.execute(f"select hf.* from history_of_fund_use hf, fundraising f where hf.fundid = f.id and f.id = '{id}';")
                history_fund = query.fetchall()

                query.execute(f"select information from submission_notes where fundid = '{id}';")
                notes = query.fetchall()



                context = {
                    'fundraising' : fundraising_all, 'category' : category_item, 'donation':donation, 'history_fund': history_fund, 'notes' : notes
                }
                query.execute("SET SEARCH_PATH TO PUBLIC;")
            
            return render(request, "view_fundraising_detail_admin.html", context)
        else:
            return redirect('/')
    else:
        return redirect('login')
## FAUZAN
def verify_fundraising_page_admin(request, id):
    if auth.is_authenticated(request):
        if request.session["user"]["type"] == 'admin':
            with connection.cursor() as query:
                query.execute("SET SEARCH_PATH TO SIDONA;")
                

                query.execute("SET SEARCH_PATH TO PUBLIC;")


            return render(request, "verify_fundraising_page_admin.html", {"id":id})
        else:
            return redirect('login')
    else:
        return redirect('login')

## FAUZAN
def verify_fundraising_admin(request, id, status):
    if auth.is_authenticated(request):
        if request.session["user"]["type"] == 'admin':
            email = request.session["user"]["email"]
            if request.method == "POST":
                notes = request.POST["note"]
                with connection.cursor() as query:
                    query.execute("SET SEARCH_PATH TO SIDONA;")
                    query.execute(f"select s.* from fundraising f, submission_notes s where s.fundid = f.id and f.id = '{id}'")
                    submission_notes = query.fetchall()

                    if len(submission_notes) == 0:
                        query.execute(
                        f"insert into submission_notes (fundid, timestamp, information) values ('{id}', current_date, '{notes}');"
                        )
                    elif len(submission_notes) != 0:
                        query.execute(f"update submission_notes set timestamp = current_date, information = '{notes}' where fundid = '{id}';")

                    if status == 'approve':
                        query.execute(f"update fundraising set verificationstatus = 'Terverifikasi', emailadmin = '{email}' where id = '{id}';")
                    query.execute("SET SEARCH_PATH TO PUBLIC;")
        else:
            return redirect('/')
        return redirect('/systemadmin/view/fundraising/all/')
    else:
        return redirect('login')

## FAUZAN
def view_fundraising_categories_admin(request):
    if auth.is_authenticated(request):
        if request.session["user"]["type"] == 'admin':
            with connection.cursor() as query:
                query.execute("SET SEARCH_PATH TO SIDONA;")
                query.execute("Select * From Category;")
                category = query.fetchall()


                context = {'category': category}
                query.execute("SET SEARCH_PATH TO PUBLIC;")


            return render(request, "view_fundraising_categories_admin.html", context)
        else:
            return redirect('/')
    else:
        return redirect('login')
## FAUZAN
def add_new_fundraising_category_admin(request):
    if auth.is_authenticated(request):
        if request.session["user"]["type"] == 'admin':
            with connection.cursor() as query:
                query.execute("SET SEARCH_PATH TO SIDONA;")
                query.execute("SELECT * FROM category ORDER BY id DESC;")

                highest_id = query.fetchone()[0]
                next_id = str(int(highest_id) + 1)
                context = {'id' : next_id}
                if request.method == "POST":
                    category_name = request.POST["category"]
                    query.execute(f"insert into category values ('{next_id}','{category_name}');")

                    query.execute("SET SEARCH_PATH TO PUBLIC;")
                    return redirect("/systemadmin/view/category/all/")
                query.execute("SET SEARCH_PATH TO PUBLIC;")
                
            
            return render(request, "add_new_fundraising_category_admin.html" , context)
        else:
            return redirect('/')
    else:
        return redirect('login')

## FAUZAN
def update_existing_fundraising_category_admin(request, id):
    if auth.is_authenticated(request):
        if request.session["user"]["type"] == 'admin':
            with connection.cursor() as query:
                query.execute("SET SEARCH_PATH TO SIDONA;")
                query.execute(f"select * from category where id = '{id}'")
                category = query.fetchall()
                context = {'category' : category}
                if request.method == "POST":
                    category_name = request.POST["category"]
                    query.execute(f"update category set categoryname = '{category_name}' where id = '{id}';")

                    query.execute("SET SEARCH_PATH TO PUBLIC;")
                    return redirect("/systemadmin/view/category/all/")
                query.execute("SET SEARCH_PATH TO PUBLIC;")
            return render(request, "update_existing_fundraising_category_admin.html",context)
        else:
            return redirect('/')
    else:
        return redirect('login')
## DANISH
def view_all_user_donation_data_admin(request):
    if auth.is_authenticated(request):
        user_type = request.session["user"]["type"]
        if user_type == "admin":
            with connection.cursor() as query:
                query.execute("SET SEARCH_PATH TO SIDONA;")
                query.execute(f'SELECT F.id, F.title, C.categoryname, P.status, D.timestamp, D.email FROM FUNDRAISING F, CATEGORY C, PAYMENT_STATUS P, DONATION D WHERE D.fundid = F.id AND F.categoryid = C.id AND D.paymentstatusid = P.id;')
                result = query.fetchall()
                cleaned_result = []
                for donation in result:
                    data = [x for x in donation]
                    data[4] = str(data[4])
                    cleaned_result.append(data)
                query.execute("SET SEARCH_PATH TO PUBLIC;")
            return render(request, "view_all_user_donation_data_admin.html", {'data': cleaned_result})
    else:
        return redirect('login')

## DANISH
def verify_donation_payment_admin(request, email, timestamp):
    if auth.is_authenticated(request):
        user_type = request.session["user"]["type"]
        if user_type == "admin":
            with connection.cursor() as query:
                query.execute("SET SEARCH_PATH TO SIDONA;")
                query.execute(f"UPDATE DONATION SET paymentstatusid = 924491015 where email = '{email}' AND timestamp = '{timestamp}';")
                ## TODO +amountoffundscollected
                query.execute(f"select amount from donation where email = '{email}' AND timestamp = '{timestamp}'")
                amount = query.fetchone()[0]
                query.execute(f"SELECT F.amountoffundscollected from fundraising F, donation D where D.timestamp='{timestamp}' AND D.email='{email}' AND D.fundid=F.id")
                
                funds = query.fetchone()
                if not funds[0]:
                    query.execute(f"update fundraising set amountoffundscollected=0 where id=(select fundid from donation where timestamp='{timestamp}' and email='{email}')")

                query.execute(f"update fundraising set amountoffundscollected=amountoffundscollected+{amount} where id=(select fundid from donation where timestamp='{timestamp}' and email='{email}')")

                query.execute("SET SEARCH_PATH TO PUBLIC;")
        else:
            return redirect('login')
    return redirect("/systemadmin/view/donations/all/")

## DANISH
def view_detail_user_donation_data_admin(request, email, timestamp):
    # yang perlu lo query (email, )
    if auth.is_authenticated(request):
        user_type = request.session["user"]["type"]
        if user_type == "admin":
            with connection.cursor() as query:
                query.execute("SET SEARCH_PATH TO SIDONA;")
                query.execute(f"SELECT D.Email, D.Timestamp, F.title, D.Amount, D.PaymentMethod, D.AnonymousStatus, D.prayer FROM DONATION D, FUNDRAISING F WHERE D.Timestamp = '{timestamp}' AND D.email = '{email}' AND D.fundid = F.id;")
                data = query.fetchone()
                query.execute("SET SEARCH_PATH TO PUBLIC;")
            
            return render(request, "view_detail_user_donation_data_admin.html", {'data': data})
    else:
        return redirect('login')
    
## DANISH
def view_proof_of_payment_user_admin(request, email,timestamp):
    if auth.is_authenticated(request):
        user_type = request.session["user"]["type"]
        if user_type == "admin":
            with connection.cursor() as query:
                query.execute("SET SEARCH_PATH TO SIDONA")
                query.execute(f"SELECT proof_of_payment FROM DONATION WHERE Email = '{email}' AND Timestamp = '{timestamp}';")
                proof = query.fetchone()
                proof_url = proof[0]
                query.execute("SET SEARCH_PATH TO PUBLIC")
            return redirect(proof_url)
    else:
        return redirect('login')
    